package net.azagwen.aza_utils.mixin;

import net.azagwen.aza_utils.impl.creative_menu.ItemGroupShelfHandler;
import net.minecraft.client.gui.screen.ingame.AbstractInventoryScreen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemGroup;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(CreativeInventoryScreen.class)
public abstract class HandleItemGroupShelves extends AbstractInventoryScreen<CreativeInventoryScreen.CreativeScreenHandler> {
    private final @Unique ItemGroupShelfHandler shelfHandler = new ItemGroupShelfHandler(this);

    @Inject(method = "setSelectedTab", at = @At("HEAD"))
    void handleSelection(ItemGroup group, CallbackInfo ci) {
        this.shelfHandler.handleSelection(group);
    }

    @Inject(method = "renderTabIcon", at = @At(value = "INVOKE_ASSIGN", target = "Lnet/minecraft/item/ItemGroup;getIcon()Lnet/minecraft/item/ItemStack;"), cancellable = true, locals = LocalCapture.CAPTURE_FAILHARD)
    void handleIcon(MatrixStack matrices, ItemGroup group, CallbackInfo ci, boolean isSelected, boolean isTopRow, int column, int u, int v, int x, int y) {
        this.shelfHandler.handlePngIcon(group, matrices, x, y, ci);
    }

    HandleItemGroupShelves(CreativeInventoryScreen.CreativeScreenHandler screenHandler, PlayerInventory playerInventory, Text text) {
        super(screenHandler, playerInventory, text);
    }
}
