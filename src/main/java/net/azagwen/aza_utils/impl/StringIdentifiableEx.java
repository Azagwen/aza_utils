package net.azagwen.aza_utils.impl;

import net.minecraft.util.StringIdentifiable;

public interface StringIdentifiableEx extends StringIdentifiable {
    int getId();
    default String getName() {
        return this.asString();
    }
}
