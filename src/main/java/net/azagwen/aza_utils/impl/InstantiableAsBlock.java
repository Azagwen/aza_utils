package net.azagwen.aza_utils.impl;

import com.google.common.collect.Maps;
import net.azagwen.aza_utils.api.registry.RegistryHolder;
import net.azagwen.aza_utils.api.registry.RegistryHolderGroup;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.util.StringIdentifiable;

import java.util.function.Function;

public interface InstantiableAsBlock {

    String getBlockName(String name);
    <S extends AbstractBlock.Settings> Block getNew(S settings);

    default <S extends AbstractBlock.Settings> Block getNew(Function<S, ? extends Block> blockConstructor, S settings) {
        return blockConstructor.apply(settings);
    }

    default <S extends AbstractBlock.Settings> RegistryHolder<Block> getHolder(String name, S settings) {
        return new RegistryHolder<>(this.getBlockName(name), () -> this.getNew(settings));
    }

    default <S extends AbstractBlock.Settings> RegistryHolder<Block> getHolder(String name, Function<S, ? extends Block> blockConstructor, S settings) {
        return new RegistryHolder<>(this.getBlockName(name), () -> this.getNew(blockConstructor, settings));
    }

    static <S extends AbstractBlock.Settings, V extends StringIdentifiable & InstantiableAsBlock> RegistryHolderGroup<Block, V> createRegistryHolderGroup(V[] variants, String name, S settings) {
        var map = Maps.<V, RegistryHolder<Block>>newLinkedHashMap();
        for (var variant : variants) {
            map.put(variant, variant.getHolder(name, settings));
        }
        return new RegistryHolderGroup<>(variants, map);
    }
}
