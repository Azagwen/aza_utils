package net.azagwen.aza_utils.impl;

import net.azagwen.aza_utils.api.creative_menu.ItemGroupTab;

public interface ItemRegistryHolderCommons<S> {
    S tab(ItemGroupTab tab);
}
