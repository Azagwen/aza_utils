package net.azagwen.aza_utils.impl.creative_menu;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.systems.RenderSystem;
import net.azagwen.aza_utils.api.creative_menu.ItemGroupShelf;
import net.azagwen.aza_utils.mixin.HandledScreenAccessor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.ingame.AbstractInventoryScreen;
import net.minecraft.client.gui.screen.ingame.CreativeInventoryScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemGroup;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

public class ItemGroupShelfHandler {
    private final AbstractInventoryScreen<CreativeInventoryScreen.CreativeScreenHandler> creativeScreen;
    public final List<ItemGroupTabWidget> tabButtons = Lists.newArrayList();
    public ItemGroupTabWidget selectedtab;

    public ItemGroupShelfHandler(AbstractInventoryScreen<CreativeInventoryScreen.CreativeScreenHandler> creativeScreen) {
        this.creativeScreen = creativeScreen;
    }

    public void handleSelection(ItemGroup group) {
        var screen = (HandledScreenAccessor) this.creativeScreen;
        var x = screen.getX();
        var y = screen.getY();

        for (var button : this.tabButtons) {
            screen.invokeRemove(button);
        }
        this.tabButtons.clear();

        if (group instanceof ItemGroupShelf shelf){
            if (shelf.getTabs().size() > 1){
                int i = 0;
                for (var tab : shelf.getTabs()) {
                    var selectTab = i;
                    var flipTab = i > 3;
                    var xOffset = flipTab ? (x + 191) : (x - 29);
                    var yOffset = flipTab ? (y + 12) + ((i - 4) * 30) : (y + 12) + (i * 30);
                    var tabWidget = new ItemGroupTabWidget(this.creativeScreen, xOffset, yOffset, flipTab, tab, (button) -> {
                        shelf.setSelectedTab(selectTab);
                        MinecraftClient.getInstance().setScreen(this.creativeScreen);
                        ((ItemGroupTabWidget) button).isSelected = true;
                        this.selectedtab = (ItemGroupTabWidget) button;
                    });

                    if (i == shelf.getSelectedTabIndex()) {
                        this.selectedtab = tabWidget;
                        tabWidget.isSelected = true;
                    }

                    this.tabButtons.add(tabWidget);
                    screen.invokeAddDrawableChild(tabWidget);
                    i++;
                }
            }
        }
    }

    public void handlePngIcon(ItemGroup group, MatrixStack matrices, int x, int y, CallbackInfo ci) {
        if (group instanceof ItemGroupShelf shelf) {
            if (shelf.getIcon().isEmpty()) {
                RenderSystem.setShaderTexture(0, shelf.getPngIcon());
                DrawableHelper.drawTexture(matrices, x, y, 0, 0, 16, 16, 16, 16);
                ci.cancel();
            }
        }
    }
}
