package net.azagwen.aza_utils.impl.creative_menu;

import com.mojang.blaze3d.systems.RenderSystem;
import net.azagwen.aza_utils.example_mod.Main;
import net.azagwen.aza_utils.api.creative_menu.ItemGroupTab;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import java.util.function.Consumer;

public class ItemGroupTabWidget extends ButtonWidget {
    private static final Identifier TEXTURE = Main.id("textures/gui/side_tabs.png");
    protected boolean isSelected = false;
    protected final boolean flipped;
    private final ItemGroupTab tab;

    ItemGroupTabWidget(Screen parent, int x, int y, boolean flipped, ItemGroupTab tab, PressAction onPress) {
        super(x, y, 33, 28, Text.translatable(tab.getTranslationKey()), onPress, new TooltipSupplier() {
            @Override
            public void onTooltip(ButtonWidget buttonWidget, MatrixStack matrixStack, int i, int j) {
                parent.renderTooltip(matrixStack, Text.translatable(tab.getTranslationKey()), i, j);
            }

            @Override
            public void supply(Consumer<Text> consumer) {
                consumer.accept(Text.translatable(tab.getTranslationKey()));
            }
        });
        this.flipped = flipped;
        this.tab = tab;

    }

    protected int getXImage(boolean isHovered) {
        return isSelected ? 2 : (isHovered ? 1 : 0);
    }

    @Override
    public void playDownSound(SoundManager soundManager) {
    }

    @Override
    public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float delta) {
        MinecraftClient minecraftClient = MinecraftClient.getInstance();
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, this.alpha);
        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.enableDepthTest();
        this.drawTexture(matrixStack, this.x, this.y, this.getXImage(this.isHoveredOrFocused()) * width + (flipped ? width * 3 : 0), 28, this.width, this.height);
        this.renderBackground(matrixStack, minecraftClient, mouseX, mouseY);

        int xIconOffset = flipped ? (this.isHoveredOrFocused() || isSelected ? 10 : 7) : (this.isHoveredOrFocused() || isSelected ? 7 : 10);
        minecraftClient.getItemRenderer().renderGuiItemIcon(tab.getIcon(), this.x + xIconOffset, this.y + 6);
        if (!(tab.getPngIcon() == null)){
            RenderSystem.setShaderTexture(0, tab.getPngIcon());
            DrawableHelper.drawTexture(matrixStack, this.x + xIconOffset, this.y + 6, 0, 0, 16, 16, 16, 16);
        }
        if (this.isHoveredOrFocused()) {
            this.renderTooltip(matrixStack, mouseX, mouseY);
        }
    }
}