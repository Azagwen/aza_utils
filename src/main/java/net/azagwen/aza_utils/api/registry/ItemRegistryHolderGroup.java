package net.azagwen.aza_utils.api.registry;

import com.google.common.collect.Maps;
import net.azagwen.aza_utils.api.creative_menu.ItemGroupTab;
import net.azagwen.aza_utils.impl.ItemRegistryHolderCommons;
import net.minecraft.item.Item;
import net.minecraft.util.StringIdentifiable;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class ItemRegistryHolderGroup<V extends StringIdentifiable> extends RegistryHolderGroup<Item, V> implements ItemRegistryHolderCommons<ItemRegistryHolderGroup<V>> {
    private final Map<V, ItemRegistryHolder> values = Maps.newLinkedHashMap();

    public ItemRegistryHolderGroup(V[] variants, Function<V, String> nameFormatter, Function<V, Supplier<Item>> futureObject) {
        super(variants, nameFormatter, futureObject);
        super.getValues().forEach((key, value) -> this.values.put(key, new ItemRegistryHolder(value.getName(), value.getFutureObject())));
    }

    @Override
    protected Map<V, ? extends RegistryHolder<Item>> getValues() {
        return this.values;
    }

    @Override
    public ItemRegistryHolderGroup<V> tab(ItemGroupTab tab) {
        return this.tab(tab, this.variants);
    }

    @SafeVarargs
    public final ItemRegistryHolderGroup<V> tab(ItemGroupTab tab, V... variants) {
        for (var variant : variants) {
            this.values.get(variant).tab(tab);
        }
        return this;
    }
}
