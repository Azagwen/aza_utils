package net.azagwen.aza_utils.api.registry;

import net.azagwen.aza_utils.api.creative_menu.ItemGroupTab;
import net.azagwen.aza_utils.impl.ItemRegistryHolderCommons;
import net.minecraft.item.Item;

import java.util.function.Supplier;

public class ItemRegistryHolder extends RegistryHolder<Item> implements ItemRegistryHolderCommons<RegistryHolder<Item>> {
    private ItemGroupTab tab;

    public ItemRegistryHolder(String name, Supplier<Item> futureObject) {
        super(name, futureObject);
    }

    @Override
    public RegistryHolder<Item> tab(ItemGroupTab tab) {
        this.tab = tab;
        return this;
    }

    @Override
    protected void onRegister(Item value) {
        super.onRegister(value);
        this.tab.addItem(value);
    }
}
