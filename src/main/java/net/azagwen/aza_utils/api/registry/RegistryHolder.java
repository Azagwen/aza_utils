package net.azagwen.aza_utils.api.registry;

import java.util.function.Supplier;

public class RegistryHolder<T>{
    private final Supplier<T> futureObject;
    private final String name;
    private T value;

    public RegistryHolder(String name, Supplier<T> futureObject) {
        this.futureObject = futureObject;
        this.name = name;
    }

    public T get() {
        return this.value;
    }

    protected void onRegister(T value) {
        this.value = value;
    }

    Supplier<T> getFutureObject() {
        return this.futureObject;
    }

    String getName() {
        return this.name;
    }
}
