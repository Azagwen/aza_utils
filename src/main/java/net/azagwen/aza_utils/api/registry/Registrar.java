package net.azagwen.aza_utils.api.registry;

import com.google.common.collect.Maps;
import net.minecraft.util.Identifier;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.registry.Registry;
import org.slf4j.Logger;

import java.util.Map;
import java.util.function.Function;

public class Registrar<T> {
    private final Registry<T> registry;
    private final Function<String, Identifier> identifierFunction;
    private final Map<Identifier, T> objects = Maps.newLinkedHashMap();

    public Registrar(Registry<T> registry, Function<String, Identifier> identifierFunction) {
        this.registry = registry;
        this.identifierFunction = identifierFunction;
    }

    public <H extends RegistryHolder<T>> RegistryHolder<T> register(H holder) {
        var object = Registry.register(this.registry, this.identifierFunction.apply(holder.getName()), holder.getFutureObject().get());
        objects.put(this.identifierFunction.apply(holder.getName()), object);
        holder.onRegister(object);
        return holder;
    }

    public <V extends StringIdentifiable, HG extends RegistryHolderGroup<T, V>> RegistryHolderGroup<T, V> register(HG holder) {
        for (var value : holder.getValues().values()) {
            this.register(value);
        }
        return holder;
    }

    public Map<Identifier, T> getObjects() {
        return this.objects;
    }

    public void printContents(Logger logger) {
        var objectSuffix = objects.size() > 1 ? "s" : "";
        logger.info("{} {}{} registered by {} using Aza's Registry Utils.", this.objects.size(), registry.getKey().getValue().getPath(), objectSuffix, identifierFunction.apply("null").getNamespace());
    }
}
