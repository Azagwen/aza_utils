package net.azagwen.aza_utils.api.registry;

import com.google.common.collect.Maps;
import net.minecraft.util.StringIdentifiable;
import org.slf4j.Logger;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Represents a group of {@link RegistryHolder}s, used to easily register multiple objects such as planks,
 * dyed blocks or items, stone cuts, or any object that share a same base with slight variants
 *
 * @param <T> The type of the object being registered and held in this group.
 * @param <V> The type of the Variant this object should use, should be convertible to an Array.
 */
public class RegistryHolderGroup<T, V extends StringIdentifiable>{
    private final Map<V, RegistryHolder<T>> values = Maps.newLinkedHashMap();
    final V[] variants;

    public RegistryHolderGroup(V[] variants, Function<V, String> nameFormatter, Function<V, Supplier<T>> futureObject) {
        this.variants = variants;
        for (var variant : variants) {
            var fullName = nameFormatter.apply(variant);
            var holder = new RegistryHolder<>(fullName, futureObject.apply(variant));
            values.put(variant, holder);
        }
    }

    public RegistryHolderGroup(V[] variants, Map<V, RegistryHolder<T>> values) {
        this.variants = variants;
        this.values.putAll(values);
    }

    public T get(V variant) {
        return this.values.get(variant).get();
    }

    public T get(V variant, boolean debug) {
        if (debug) {
            System.out.println("this statement is being printed by an item tab!");
        }
        return this.get(variant);
    }

    protected Map<V, ? extends RegistryHolder<T>> getValues() {
        return this.values;
    }

    public void ping(Logger logger) {
        logger.info(this.values.toString());
    }
}
