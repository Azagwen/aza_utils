package net.azagwen.aza_utils.api.pairing;

public record Pair<A, B>(A a, B b) {
}
