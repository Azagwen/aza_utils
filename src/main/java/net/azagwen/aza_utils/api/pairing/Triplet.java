package net.azagwen.aza_utils.api.pairing;

public record Triplet<A, B, C>(A a, B b, C c) {
}
