package net.azagwen.aza_utils.api.pairing;

public record Quadruplet<A, B, C, D>(A a, B b, C c, D d) {
}
