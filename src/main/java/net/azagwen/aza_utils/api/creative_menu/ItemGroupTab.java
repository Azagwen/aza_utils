package net.azagwen.aza_utils.api.creative_menu;

import com.google.common.collect.Lists;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Supplier;

public class ItemGroupTab {
    private final Supplier<ItemStack> iconSupplier; // Supplier to avoid NPE at init
    private final @Nullable Identifier pngIcon;
    private final String name;
    private final List<Item> items = Lists.newArrayList();
    private ItemGroupShelf shelf;
    private ItemStack iconStack;

    private ItemGroupTab(Supplier<ItemStack> iconStack, Identifier pngIcon, String name) {
        this.iconSupplier = iconStack;
        this.name = name;
        this.iconStack = ItemStack.EMPTY;
        this.pngIcon = pngIcon;
    }

    public ItemGroupTab(Supplier<ItemStack> iconStack, String name) {
        this(iconStack, null, name);
    }

    public ItemGroupTab(Identifier pngIcon, String name) {
        this(() -> ItemStack.EMPTY, pngIcon, name);
    }

    void setShelf(@NotNull ItemGroupShelf shelf) {
        this.shelf = shelf;
    }

    public void addItem(ItemConvertible item) {
        this.items.add(item.asItem());
    }

    public List<Item> getItems() {
        return this.items;
    }

    public boolean contains(Item item) {
        return this.items.contains(item);
    }

    public boolean contains(ItemStack stack) {
        return this.contains(stack.getItem());
    }

    public ItemStack getIcon() {
        if (this.iconStack.isEmpty()) {
            this.iconStack = this.iconSupplier.get();
        }
        return iconStack;
    }

    public @Nullable Identifier getPngIcon() {
        return pngIcon;
    }

    public String getTranslationKey() {
        return String.format("itemGroup.%s.%s.%s", this.shelf.getIdentifier().getNamespace(), this.shelf.getIdentifier().getPath(), this.name);
    }
}
