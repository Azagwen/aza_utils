package net.azagwen.aza_utils.api.creative_menu;

import com.google.common.collect.Lists;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.Nullable;
import org.quiltmc.qsl.item.group.impl.ItemGroupExtensions;

import java.util.List;
import java.util.function.Supplier;

public class ItemGroupShelf extends ItemGroup {
    private final Supplier<ItemStack> iconStack;
    private final @Nullable Identifier pngIcon;
    private final Identifier identifier;
    private final List<ItemGroupTab> tabs = Lists.newArrayList();
    private ItemGroupTab selectedTab;

    public ItemGroupShelf(Supplier<ItemStack> iconStack, Identifier id, ItemGroupTab... tabs) {
        super(createIndex(), String.format("%s.%s", id.getNamespace(), id.getPath()));
        this.iconStack = iconStack;
        this.pngIcon = null;
        this.identifier = id;
        this.populateTabs(tabs);
    }

    public ItemGroupShelf(Identifier pngIcon, Identifier id, ItemGroupTab... tabs) {
        super(createIndex(), "");
        this.iconStack = () -> ItemStack.EMPTY;
        this.pngIcon = pngIcon;
        this.identifier = id;
        this.populateTabs(tabs);
    }

    private void populateTabs(ItemGroupTab... tabs) {
        for (var tab : tabs) {
            tab.setShelf(this);
            this.tabs.add(tab);
        }
        this.selectedTab = this.tabs.get(0);
    }

    @SuppressWarnings("UnstableApiUsage")
    private static int createIndex() {
        ((ItemGroupExtensions) GROUPS[0]).quilt$expandArray();
        return GROUPS.length - 1;
    }

    @Override
    public void appendStacks(DefaultedList<ItemStack> stacks) {
        for (Item item : Registry.ITEM) {
            if (this.getSelectedTab().contains(item)) {
                stacks.add(new ItemStack(item));
            }
        }
    }

    @Override
    public ItemStack createIcon() {
        return this.iconStack.get();
    }

    @Override
    public Text getTranslationKey() {
        return Text.translatable(String.format("itemGroup.%s.%s", this.identifier.getNamespace(), this.identifier.getPath()));
    }

    public Identifier getPngIcon() {
        return this.pngIcon;
    }

    Identifier getIdentifier() {
        return this.identifier;
    }

    public List<ItemGroupTab> getTabs() {
        return this.tabs;
    }

    ItemGroupTab getSelectedTab() {
        return this.selectedTab;
    }

    public int getSelectedTabIndex() {
        return this.tabs.indexOf(this.selectedTab);
    }

    public void setSelectedTab(int index) {
        this.selectedTab = this.tabs.get(index);
    }
}
