package net.azagwen.aza_utils.api;

public class AzaUtils {

    public static boolean isIntInArray(int i, int... array) {
        for (var value: array) {
            if (value == i) {
                return true;
            }
        }
        return false;
    }
}
