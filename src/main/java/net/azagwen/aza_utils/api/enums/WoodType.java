package net.azagwen.aza_utils.api.enums;

import com.google.common.collect.Lists;
import net.azagwen.aza_utils.api.AzaUtils;
import net.azagwen.aza_utils.impl.StringIdentifiableEx;
import net.minecraft.block.MapColor;

import java.util.ArrayList;

/**
 * Similar to {@link net.minecraft.util.DyeColor}, but for Wood types, can be expanded upon by extending it.
 */
public enum WoodType implements StringIdentifiableEx {
    OAK(0, "oak", MapColor.OAK_TAN, MapColor.SPRUCE_BROWN),
    SPRUCE(1, "spruce", MapColor.SPRUCE_BROWN, MapColor.BROWN),
    BIRCH(2, "birch", MapColor.PALE_YELLOW, MapColor.OFF_WHITE),
    JUNGLE(3, "jungle", MapColor.DIRT_BROWN, MapColor.SPRUCE_BROWN),
    ACACIA(4, "acacia", MapColor.ORANGE, MapColor.STONE_GRAY),
    DARK_OAK(5, "dark_oak", MapColor.BROWN, MapColor.BROWN),
    CRIMSON(6, "crimson", MapColor.DULL_PINK, MapColor.DULL_PINK),
    WARPED(7, "warped", MapColor.DARK_AQUA, MapColor.DARK_AQUA),
    MANGROVE(8, "mangrove", MapColor.RED, MapColor.SPRUCE_BROWN);

    final int id;
    final String name;
    final MapColor barkColor;
    final MapColor plankColor;
    final ArrayList<WoodType> overworldValues = Lists.newArrayList();
    final ArrayList<WoodType> netherValues = Lists.newArrayList();
    final ArrayList<WoodType> valuesMinusOak = Lists.newArrayList();

    WoodType(int id, String name, MapColor barkColor, MapColor plankColor) {
        this.id = id;
        this.name = name;
        this.barkColor = barkColor;
        this.plankColor = plankColor;

        //Lists
        if (id != 0) this.valuesMinusOak.add(this);
        (AzaUtils.isIntInArray(id, 6, 7) ? this.netherValues : this.overworldValues).add(this);
    }

    public ArrayList<WoodType> getOverworldValues() {
        return this.overworldValues;
    }

    public ArrayList<WoodType> getNetherValues() {
        return this.netherValues;
    }

    public ArrayList<WoodType> getValuesMinusOak() {
        return this.valuesMinusOak;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String asString() {
        return this.name;
    }

    public MapColor getBarkColor() {
        return this.barkColor;
    }

    public MapColor getPlankColor() {
        return this.plankColor;
    }
}
