package net.azagwen.aza_utils.api.enums.block_instancers;

import net.azagwen.aza_utils.api.block.StairsBlockEx;
import net.azagwen.aza_utils.impl.InstantiableAsBlock;
import net.azagwen.aza_utils.impl.StringIdentifiableEx;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.WallBlock;

import java.util.function.Function;

public enum StoneCuts implements StringIdentifiableEx, InstantiableAsBlock {
    NONE(0, "none", "%s", Block::new),
    NONE_SLAB(1, "none_slab", "%s_slab", SlabBlock::new),
    NONE_STAIRS(2, "none_stairs", "%s_stairs", StairsBlockEx::new),
    COBBLED(3, "cobbled", "cobbled_%s", Block::new),
    COBBLED_SLAB(4, "cobbled_slab", "cobbled_%s_slab", SlabBlock::new),
    COBBLED_STAIRS(5, "cobbled_stairs", "cobbled_%s_stairs", StairsBlockEx::new),
    COBBLED_WALL(6, "cobbled_wall", "cobbled_%s_wall", WallBlock::new),
    BRICKS(7, "bricks", "%s_bricks", Block::new),
    BRICKS_SLAB(8, "bricks_slab", "%s_bricks_slab", SlabBlock::new),
    BRICKS_STAIRS(9, "bricks_stairs", "%s_bricks_stairs", StairsBlockEx::new),
    BRICKS_WALL(10, "bricks_wall", "%s_bricks_wall", WallBlock::new),
    BRICKS_CRACKED(11, "bricks_cracked", "cracked_%s_bricks", Block::new),
    BRICKS_MOSSY(12, "bricks_mossy", "mossy_%s_bricks", Block::new),
    BRICKS_CHISELED(13, "bricks_chiseled", "chiseled_%s_bricks", Block::new);

    final int id;
    final String name;
    final Function<String, String> blockName;
    final Function<AbstractBlock.Settings, Block> blockConstructor;
    StoneCuts(int id, String name, String blockName, Function<AbstractBlock.Settings, Block> blockConstructor) {
        this.id = id;
        this.name = name;
        this.blockConstructor = blockConstructor;
        this.blockName = (n) -> String.format(blockName, n);
    }

    @Override
    public Block getNew(AbstractBlock.Settings settings) {
        return this.blockConstructor.apply(settings);
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String asString() {
        return this.name;
    }

    @Override
    public String getBlockName(String name) {
        return this.blockName.apply(name);
    }
}
