package net.azagwen.aza_utils.api.enums.block_instancers;

import net.azagwen.aza_utils.api.block.StairsBlockEx;
import net.azagwen.aza_utils.impl.InstantiableAsBlock;
import net.azagwen.aza_utils.impl.StringIdentifiableEx;
import net.minecraft.block.*;

import java.util.function.Function;

public enum WoodCuts implements StringIdentifiableEx, InstantiableAsBlock {
    LOG(0, "log", "%s_log", PillarBlock::new),
    WOOD(1, "wood", "%s_wood", PillarBlock::new),
    PLANKS(2, "planks", "%s_planks", SlabBlock::new),
    SLAB(3, "slab", "%s_slab", SlabBlock::new),
    STAIRS(4, "stairs", "%s_stairs", StairsBlockEx::new),
    FENCE(5, "fence", "%s_fence", FenceBlock::new),
    FENCE_GATE(6, "fence_gate", "%s_fence_gate", FenceGateBlock::new),
    DOOR(7, "door", "%s_door", DoorBlock::new),
    TRAPDOOR(8, "trapdoor", "%s_trapdoor", TrapdoorBlock::new),
    PRESSURE_PLATE(9, "pressure_plate", "%s_pressure_plate", (settings) -> new PressurePlateBlock(PressurePlateBlock.ActivationRule.EVERYTHING, settings)),
    BUTTON(10, "button", "%s_button", WoodenButtonBlock::new);

    final int id;
    final String name;
    final Function<String, String> blockName;
    final Function<AbstractBlock.Settings, Block> blockConstructor;
    WoodCuts(int id, String name, String blockName, Function<AbstractBlock.Settings, Block> defaultBlockConstructor) {
        this.id = id;
        this.name = name;
        this.blockConstructor = defaultBlockConstructor;
        this.blockName = (n) -> String.format(blockName, n);
    }

    @Override
    public Block getNew(AbstractBlock.Settings settings) {
        return this.blockConstructor.apply(settings);
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String asString() {
        return this.name;
    }

    @Override
    public String getBlockName(String name) {
        return this.blockName.apply(name);
    }
}
