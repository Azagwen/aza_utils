package net.azagwen.aza_utils.example_mod;

import net.azagwen.aza_utils.api.enums.block_instancers.StoneCuts;
import net.azagwen.aza_utils.api.enums.block_instancers.WoodCuts;
import net.azagwen.aza_utils.api.registry.RegistryHolderGroup;
import net.azagwen.aza_utils.api.registry.Registrar;
import net.azagwen.aza_utils.api.registry.RegistryHolder;
import net.azagwen.aza_utils.impl.InstantiableAsBlock;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.util.DyeColor;
import net.minecraft.util.registry.Registry;
import org.quiltmc.qsl.block.extensions.api.QuiltBlockSettings;

public class BlockRegistry {
    private static final Registrar<Block> BLOCKS = new Registrar<>(Registry.BLOCK, Main::id);
    public static final RegistryHolder<Block> TEST_BLOCK = BLOCKS.register(new RegistryHolder<>("test_block", () -> new Block(QuiltBlockSettings.of(Material.STONE))));
    public static final RegistryHolderGroup<Block, DyeColor> COLORED_TEST_BLOCK = BLOCKS.register(new RegistryHolderGroup<>(DyeColor.values(),
            (variant) -> String.format("%s_test_block", variant),
            (variant) -> () -> new Block(QuiltBlockSettings.of(Material.STONE, variant.getMapColor()))
    ));
    public static final RegistryHolderGroup<Block, StoneCuts> TESTANITE_BLOCKS = BLOCKS.register(
            InstantiableAsBlock.createRegistryHolderGroup(StoneCuts.values(), "testanite", QuiltBlockSettings.of(Material.STONE))
    );
    public static final RegistryHolderGroup<Block, WoodCuts> EXAMPLE_WOOD_BLOCKS = BLOCKS.register(
            InstantiableAsBlock.createRegistryHolderGroup(WoodCuts.values(), "example_wood", QuiltBlockSettings.of(Material.WOOD))
    );

    public static void init() {
        BLOCKS.printContents(Main.LOGGER);
    }
}
