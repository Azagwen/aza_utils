package net.azagwen.aza_utils.example_mod;

import net.azagwen.aza_utils.api.creative_menu.ItemGroupShelf;
import net.azagwen.aza_utils.api.creative_menu.ItemGroupTab;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.loader.api.QuiltLoader;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** TODO: Fix multiple ItemGroupShelf & Tab issues ->
 * - Fix icons not rendering in Tabs
 */


public class Main implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("Aza Utils");
	public static final String ID = "aza_utils";
	public static boolean ENABLE_EXAMPLES = false;

	public static Identifier id(String path) {
		return new Identifier(ID, path);
	}

	public static void enableExamples() {
		ENABLE_EXAMPLES = true;
	}

	public static void disableExamples() {
		ENABLE_EXAMPLES = false;
	}

	public static ItemStack getIconStack(DyeColor dyeColor) {
		return new ItemStack(ItemRegistry.COLORED_TEST_BLOCK.get(dyeColor, false));
	}

	public static ItemGroupTab TAB_1 = new ItemGroupTab(id("textures/gui/aza_logo.png"), "first_tab");
	public static ItemGroupTab TAB_2 = new ItemGroupTab(() -> getIconStack(DyeColor.RED), "second_tab");
	public static ItemGroupTab TAB_3 = new ItemGroupTab(() -> getIconStack(DyeColor.GREEN), "third_tab");
	public static ItemGroupTab TAB_4 = new ItemGroupTab(() -> getIconStack(DyeColor.BLUE), "fourth_tab");
	public static ItemGroup EXAMPLE_GROUP;
	public static ItemGroup EXAMPLE_GROUP_2;

	@Override
	public void onInitialize(ModContainer mod) {
		if ((QuiltLoader.isDevelopmentEnvironment() && ENABLE_EXAMPLES) || QuiltLoader.getAllMods().size() < 68) {
			EXAMPLE_GROUP = new ItemGroupShelf(id("textures/gui/aza_logo.png"), id("example_group"), TAB_1, TAB_2, TAB_3, TAB_4);
			EXAMPLE_GROUP_2 = new ItemGroupShelf(id("textures/gui/aza_logo.png"), id("example_group"), TAB_1);
			BlockRegistry.init();
			ItemRegistry.init();
			LOGGER.info("Aza Utils Example Mod ready ✅");
		}
	}
}
