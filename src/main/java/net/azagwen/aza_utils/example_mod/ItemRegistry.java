package net.azagwen.aza_utils.example_mod;

import net.azagwen.aza_utils.api.enums.block_instancers.StoneCuts;
import net.azagwen.aza_utils.api.enums.block_instancers.WoodCuts;
import net.azagwen.aza_utils.api.registry.*;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.DyeColor;
import net.minecraft.util.registry.Registry;

public class ItemRegistry {
    private static final Registrar<Item> ITEMS = new Registrar<>(Registry.ITEM, Main::id);
    public static final RegistryHolder<Item> TEST_BLOCK = ITEMS.register(new ItemRegistryHolder("test_block",
            () -> new BlockItem(BlockRegistry.TEST_BLOCK.get(), new Item.Settings()))
            .tab(Main.TAB_1)
    );
    public static final RegistryHolderGroup<Item, DyeColor> COLORED_TEST_BLOCK = ITEMS.register(new ItemRegistryHolderGroup<>(DyeColor.values(),
            (variant) -> String.format("%s_test_block", variant),
            (variant) -> () -> new BlockItem(BlockRegistry.COLORED_TEST_BLOCK.get(variant), new Item.Settings()))
            .tab(Main.TAB_1)
            .tab(Main.TAB_2, DyeColor.RED, DyeColor.ORANGE, DyeColor.YELLOW, DyeColor.MAGENTA)
            .tab(Main.TAB_3, DyeColor.GREEN, DyeColor.LIME)
            .tab(Main.TAB_4, DyeColor.BLUE, DyeColor.LIGHT_BLUE, DyeColor.CYAN, DyeColor.PURPLE)
    );
    public static final RegistryHolderGroup<Item, StoneCuts> TESTANITE_BLOCK_ITEMS = ITEMS.register(new ItemRegistryHolderGroup<>(StoneCuts.values(),
            (variant) -> variant.getBlockName("testanite"),
            (variant) -> () -> new BlockItem(BlockRegistry.TESTANITE_BLOCKS.get(variant), new Item.Settings()))
            .tab(Main.TAB_1)
    );
    public static final RegistryHolderGroup<Item, WoodCuts> EXAMPLE_WOOD_BLOCK_ITEMS = ITEMS.register(new ItemRegistryHolderGroup<>(WoodCuts.values(),
            (variant) -> variant.getBlockName("example_wood"),
            (variant) -> () -> new BlockItem(BlockRegistry.EXAMPLE_WOOD_BLOCKS.get(variant), new Item.Settings()))
            .tab(Main.TAB_1)
    );

    public static void init() {
        ITEMS.printContents(Main.LOGGER);
    }
}
